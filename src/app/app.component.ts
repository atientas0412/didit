import { Component,Inject } from '@angular/core';
import { NotificationService } from './services/notifications/notifications.abstract.service'

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

	notificationService : NotificationService;

	notificationsActive : boolean;

	constructor(@Inject('NotificationService') notificationService : any){
		this.notificationService = notificationService;
		this.checkPermission();
	}

	checkPermission(){
		this.notificationService.isPermitted().then(hasPermission => {
			console.log(hasPermission);
			this.notificationsActive = hasPermission;
		});
	}

	toggleNotificationPermission(){
		this.notificationService.togglePermission()
				.then(()=> this.checkPermission())
				.catch(console.log);
		
		// 
	}
}
