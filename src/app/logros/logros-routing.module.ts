import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LogrosComponent } from './logros.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: LogrosComponent }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class LogrosRoutingModule { }
