import { Injectable } from '@angular/core';

export interface NotificationService {
	requestPermission();
	saveSubscription(data : any);
	isPermitted() : Promise<any>;
	removePermission();
	togglePermission();
}