import { Injectable,Inject } from '@angular/core';
import { Logro } from '../logros/logro';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { DBService} from './database.abstract.service';

export const LogrosUrl = "/api";


@Injectable()
export class LogrosService {
	dbService : DBService;
	constructor(private http : Http, @Inject('DBService') dbService : any) {
		this.dbService = dbService;
	}

	push(logro){
		console.log("Here we goooo!");
		console.log(logro);
		return this.http.post(LogrosUrl+"/",logro)
						.map(this.parseLogro)
						.catch(this.handleError);
	}

	getSaved(){
		return this.dbService.get().logros
								.orderBy("id")
			          .reverse()
			          .toArray();
	}

	getLatestSavedID() : Promise<string>{
		let lastLogroPromise = this.dbService.get().logros
								.orderBy("serverID")
			          .reverse()
			          .toArray();	
		
		return new Promise(function(resolve,reject){
			lastLogroPromise.then((lastLogro)=>{
				if(lastLogro.length < 1){
					resolve("");
				}else{
					resolve(lastLogro[0].serverID);	
				}
				
			}).catch(err => console.log(err));			
		});		

	}

	get(){
		let promiseFirstSaved = this.getLatestSavedID();

		let getFromRemotePromise = promiseFirstSaved
																.then((id)=> this.getRemote(id))
																.catch(() => this.getRemote());
		
		return getFromRemotePromise;				

	}

	getRemote(id=""){
		let from = (id.length > 0) ? "/?from="+id : "";
		return this.http.get(LogrosUrl+from)
          .map(this.parseLogros)
          .catch(this.handleError);
	}

	getRemoteOldest(id=""){
		return this.http.get(LogrosUrl+"/?since="+id)
          .map(this.parseLogros)
          .catch(this.handleError);
	}

	handleError(error: Response | any){
		console.log(error);
		return Observable.throw("Algo salio mal");
	}

	parseLogro(response : Response) : Logro{
		let logroJSON = response.json();
		return new Logro(logroJSON.title,logroJSON.author,logroJSON.description)
	}

	parseLogros(response : Response){
		let data = response.json();

		return data.map(logroJSON => new Logro(logroJSON.title,logroJSON.author,logroJSON.description,logroJSON['_id']));
	}
}