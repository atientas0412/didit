import { Injectable } from '@angular/core';
import { Logro } from '../logros/logro';
import Dexie from 'dexie';
import { DBService } from './database.abstract.service';

interface ILogro{
	id ?: number;
	title : string;
	author : string;
	description ?: string;
	serverID ?: string;
}

@Injectable()
export class DBServiceBrowser implements DBService{
	database : LogrosDatabase;
	
	constructor() {
		this.database = new LogrosDatabase();
	}

	get(){return this.database;}

	sync(logros : Logro[]){

		for (let i = logros.length -1; i >= 0; --i) {
			let logro = logros[i];
			this.database.logros.add(logro);
		};

		this.deleteOld().then(([lasts,deleted])=>{});
		
	}

	deleteOld(){
		let lastPromise = this.database.logros
													.orderBy("id").reverse().limit(10).toArray();
		let removePromises = lastPromise.then((results)=>{
			let lastLogro = results[results.length - 1];
			// console.log(results);
			// console.log(lastLogro);
			return this.database.logros.where("id").below(lastLogro.id).delete();
		});

		return Promise.all([lastPromise]);
	}
}


class LogrosDatabase extends Dexie{
	
	logros : Dexie.Table<ILogro,number>;

	constructor(){
		super('logrosDatabase');
		this.version(1).stores({
			logros: '++id,title,author,description'
		});
		this.version(2).stores({
			logros: '++id,title,author,description,serverID'
		});

		this.logros.mapToClass(Logro);
	}
}